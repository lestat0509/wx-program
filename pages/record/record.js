// pages/reocrd/record.js
import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum: 1,
    recordItems:[
      
    ],
    supportItems:[
      
    ],
  },

  gotoDetail:function(e){
    var id = e.currentTarget.dataset['index']
    wx.navigateTo({
      url: '../orderDetail/orderDetail?scene=' + id,
    })
  },
  getRecordList: function (pageNum) {
    let that = this
    var branch_id = wx.getStorageSync('branch')
    network.get({
      url: 'branches/' + branch_id +'/orders', data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 10 // 每页条数 默认10
        }

      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          if (pageNum == 1) {
            that.setData({ recordItems: data.list })
          } else {
            that.setData({ recordItems: that.data.recordItems.concat(data.list) })
          }
        } else {
          if (pageNum > 1) {
            that.setData({ pageNum: that.data.pageNum - 1 })
          }
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getRecordList(1)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getRecordList(1)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
    this.getRecordList(this.data.pageNum)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})