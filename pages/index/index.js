import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoris:[],
    wawaType:1,
    pageNum:1,
    allPageNum:1,
    showShouquan:false,
    bannerList: [
      {
        image_url: '../image/banner_default.png',
        json_data: 'hollekitty仔粉红顽皮豹娃娃可爱',
        id: 30 
      }
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 7000,
    duration: 1000,
    currentTab: 0,
    allItems: [
     ],
    recommendItems:[
     ],
    dongtaiItems:[   
    ],
  },
  
  bindGetUserInfo: function (e) {
    var that = this
    this.setData({showShouquan:false})
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          let code = res.code
          wx.getUserInfo({
            success: function (res) {
              network.post({
                url: 'auth/login', data: {
                  code: code,
                  encryptedData: res.encryptedData,
                  iv: res.iv
                }
              })
                .then(({ code, data }) => {
                  if (code == 0) {
                    app.globalData.userInfo = data.userInfo
                    app.globalData.token = data.token
                    wx.setStorageSync('token', data.token)
                    wx.setStorageSync('userId', data.userInfo.uid)
                    that.getBannerList()
                    that.getOrderNew()
                    that.getWawaList(1, 1)
                    that.getWawaList(0, 1)
                  } 
                })
            }
          })
        }
      }
    })
    wx.showModal({
      content: '请选择门店',
      success: function (res) {
        if (res.confirm) {
          wx.navigateTo({
            url: '../local/local',
          })
        }
      }
    })
  },

  //滑动切换
  swiperTab: function (e) {
    var that = this;
    console.log(e.detail.current)
    that.setData({
      currentTab: e.detail.current
    });
    if(e.detail.current == 0){
      this.setData({wawaType:1})
      this.setData({
        view: {
          Height: this.data.recommendItems.length / 2 * 300 + 20
        }
      })
    }else{
      this.setData({ wawaType: 0 })
      this.setData({
        view: {
          Height: this.data.allItems.length / 2 * 300 + 20
        }
      })
    }
  },
  //点击切换
  clickTab: function (e) {

    var that = this;
    
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      
      that.setData({
        currentTab: e.target.dataset.current
      })
      if (e.detail.current == 0) {
        this.setData({ wawaType: 1 })
        this.setData({
          view: {
            Height: this.data.recommendItems.length / 2 * 300 + 20
          }
        })
      } else {
        this.setData({ wawaType: 0 })
        this.setData({
          view: {
            Height: this.data.allItems.length / 2 * 300 + 20
          }
        })
      }
    }
  },


  gotoWawaKind:function(e){
    var index = e.currentTarget.dataset['index'];
    var id = e.currentTarget.dataset['id']
    wx.navigateTo({
      url: '../wawakind/wawakind?name=' +index + "&id=" + id ,
    })
  },
  gotoDongtai:function(){
    wx.navigateTo({
      url: '../dynamic/dynamic',
    })
  },
  gotoGoodsDetail:function(e){
    let id = e.currentTarget.dataset['id']
    wx.navigateTo({
      url: '../awardDetail/awardDetail?scene=' + id,
    })
  },

  getBannerList:function(){
    let that = this
    if (wx.getStorageSync('branch')){
      var branch_id = wx.getStorageSync('branch')
    }else{
      var branch_id = 1
    }
      network.get({
        url: 'branches/' + branch_id + '/banners'
      })
        .then(({ code, data }) => {
          console.log("data")
          console.log(data)
          if (code == 0) {
            if (data.list && data.list.length > 0) {
              that.setData({ bannerList: data.list })
            } else {
              that.setData({ bannerList: this.data.bannerList })
            }
          }
        })
  },
  gotoPage: function (e) {
    let category = e.currentTarget.dataset['category']
    let json_data = e.currentTarget.dataset['json_data']
    if (category == "detail") {
      wx.navigateTo({
        url: '../awardDetail/awardDetail?scene=' + json_data,
      })
    } else if (category == "link") {
      wx.navigateTo({
        url: '../poster/poster?scene=' + json_data,
      })
    } else if (category == "page") {
      wx.navigateTo({
        url: json_data,
      })
    }
  },

  gotoCategory:function(){
    wx.navigateTo({
      url: '../promotion/promotion',
    })
  },

  getWawaList:function(type,pageNum){
    let that = this
    if (wx.getStorageSync('branch')) {
      var branch_id = wx.getStorageSync('branch')
    } else {
      var branch_id = 1
    }
      network.get({
        url: 'branches/' + branch_id + '/goods', data: {
          pagination: { // 分页参数 如果不传返回全部
            page: pageNum, // 当前页
            per_page: 10 // 每页条数 默认10
          },
          filter: {
            recommended: type, // 获取推荐列表，不传或者传0表示全部
          } // 过滤参数
        }
      })
        .then(({ code, data }) => {
          console.log("data")
          console.log(data)
          if (code == 0) {
            if (data.list && data.list.length > 0) {
              if (data.pageNo == 1) {
                if (type == 1) {
                  that.setData({
                    recommendItems: data.list
                  })
                } else {
                  that.setData({
                    allItems: data.list
                  })
                }
              } else {
                if (type == 1) {
                  that.setData({
                    recommendItems: that.data.recommendItems.concat(data.list)
                  })
                } else {
                  that.setData({
                    allItems: that.data.allItems.concat(data.list)
                  })
                }
              }
              if (type == 1) {
                that.setData({
                  view: {
                    Height: that.data.recommendItems.length / 2 * 250 + 20
                  }
                })
              } else {
                that.setData({
                  view: {
                    Height: that.data.allItems.length / 2 * 250 + 20
                  }
                })
              }
            }
            wx.stopPullDownRefresh()
          } else {
            this.setData({ pageNum: pageNum - 1 })
            wx.stopPullDownRefresh()
          }
        })
  },
  getOrderNew:function(){
    let that = this
    network.get({
      url: 'orders/latest',data:{
        pagination: { // 分页参数 如果不传返回全部
          page: 1, // 当前页
          per_page: 10 // 每页条数 默认10
        }
      }
    })
      .then(({ code, data }) => {
        
        console.log(data)
        if (code == 0) {
          if (data.list && data.list.length > 0) {
            for (var i=0;i<data.list.length;i++)
            {var dongtaiTime=data.list[i].order_time.slice(5,16);
            data.list[i].order_time =dongtaiTime
            }
            that.setData({dongtaiItems:data.list})
          }
        }
      })
  },
  gotoSearch:function(){
    wx.navigateTo({
      url: '../search/search',
    })
  },

  getCategoris:function(){
    let that = this
    network.get({
      url: 'categories'
    })
      .then(({ code, data }) => {
        this.setData({
          categoris:data
        })
        if(data!= null&&data.length > 0){
          app.globalData.categoris = data
        }
        
      })
  },

  goLocal: function () {
    wx.navigateTo({
      url: '../local/local',
    })
  },

  loadData:function(){
    this.getBannerList();
    this.getOrderNew()
    this.getWawaList(1, 1)
    this.getWawaList(0, 1)
  }, 

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;                                                             
    this.getCategoris()
    this.loadData()
    this.setData({
      view: {
        Height: 320
      }
    })
    wx.getSetting({
      success: function (res) {
        
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          // // 登录
          console.log("goodoooooo")
          if (app.globalData.userInfo != null){
            that.getBannerList();
            that.getOrderNew()
            that.getWawaList(1, 1)
            that.getWawaList(0, 1)
            if(options.url&& options.url!= null){
              var scene = "?url=" + options.url + "&uid=" + options.uid + "&name=" + options.name + "&doll=" + options.doll
              wx.navigateTo({
                url: '../supportShare/supportShare' + scene,
              })
            }
            console.log("goodoooooo1")
          }else{
            console.log("goodoooooo2")
            if (!app.userCallBack) {
              app.userCallBack = (data) => {
                console.log("goodoooooo3")
                that.getBannerList()
                that.getOrderNew()
                that.getWawaList(1, 1)
                that.getWawaList(0, 1)
                if (options.url && options.url != null) {
                  var scene = "?url=" + options.url + "&uid=" + options.uid + "&name=" + options.name + "&doll=" + options.doll
                  wx.navigateTo({
                    url: '../supportShare/supportShare' + scene,
                  })
                }
              }
            }
          }

        }else{
          that.setData({ showShouquan:true})
        }
      }
    })
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.setNavigationBarTitle({
      title: wx.getStorageSync('branch_name'),
    })
  },
    

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (e) {
    this.getBannerList();
    this.getWawaList(1, 1);
    this.getWawaList(0, 1);
    this.onReady();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // wx.showNavigationBarLoading() //在标题栏中显示加载
    this.getBannerList();
    this.getOrderNew()
    this.setData({ pageNum:1})
    this.getWawaList(this.data.wawaType,1)
    
    console.log("wawa")
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if(this.data.wawaType == 0){
      this.setData({ allPageNum: this.data.allPageNum + 1 })
      this.getWawaList(this.data.wawaType, this.data.allPageNum)
    }else{
    this.setData({pageNum:this.data.pageNum+1})
      this.getWawaList(this.data.wawaType, this.data.pageNum)
    }
    
    console.log("wawa")
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})