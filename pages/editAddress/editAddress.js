var address = require('../../utils/city.js')
import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addressDetail: '',
    userName: '',
    userPhone: '',
    provinces: [],
    citys: [],
    areas: [],
    areaInfo: '请选择省市区',
    animationAddressMenu: {},
    animationAddressMenuMendian: {},
    addressMenuIsShow: false,
    value: [0, 0, 0],
    areaAddress: '',
    address_id:'0',
    switches:false,
  },

  userNameInput: function (e) {
    this.setData({
      userName: e.detail.value
    })
  },
  addressDetailInput: function (e) {
    this.setData({
      addressDetail: e.detail.value
    })
  },
  switchChange: function (e) {
    this.setData({
      switches: e.detail.value
    })
  },
  
  //手机号验证
  userPhoneInput: function (e) {
    var userPhone = e.detail.value;
    this.setData({
      userPhone: e.detail.value
    })
  },

  gotoExchange: function () {
    if (this.data.addressDetail.length == 0) {
      wx.showToast({ title: '请填写收货详细地址', icon: 'none' })
      return
    }
    if (this.data.userName.length == 0) {
      wx.showToast({ title: '请填写收件人', icon: 'none' })
      return
    }
    if (this.data.userPhone.length == 0) {
      wx.showToast({ title: '请填写收件人联系电话', icon: 'none' })
      return
    }
    if (this.data.userPhone.length != 11) {
      wx.showToast({ title: '手机号格式错误', icon: 'none' })
      return
    }
    var myreg = /^(((13[0-9]{1})|159|153)+\d{8})$/;
    if (!(/^1[3|4|5|7|8][0-9]\d{8,11}$/.test(this.data.userPhone))) {
      wx.showToast({ title: '手机号格式错误', icon: 'none' })
      return
    }
    this.requstExchange()
  },
  requstExchange: function () {
    let that = this
    var addressId = this.data.address_id;
    var value = this.data.value;
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    network.put({
      url: 'address/' + addressId, data: {
        name: that.data.userName, // 收件人名称
        phone: that.data.userPhone, // 收件人电话
        is_default: that.data.switches, 
        province:that.data.provinces[value[0]].name,
        city: that.data.citys[value[1]].name,
        county: that.data.areas[value[2]].name,
        address: that.data.areaInfo+that.data.addressDetail, // 地址信息
      },
    })
      .then(({ code, data }) => {
        if (code == 0) {
          wx.showToast({ title: '编辑成功', icon: 'success' })
          setTimeout(function () {
            wx.navigateBack()
          }, 2000) 
        } else {
          console.log(code)
          wx.showToast({ title: '系统错误', icon: 'none' })
        }
      })
  },

  // 点击所在地区弹出选择框
  selectDistrict: function () {
    var that = this
    that.startAddressAnimation(true)
  },
  // 执行动画
  startAddressAnimation: function (isShow) {
    console.log(isShow)
    var that = this
    var animation = wx.createAnimation({
      duration: 100,
      timingFunction: 'ease',
    })
    if (isShow) {
      // vh是用来表示尺寸的单位，高度全屏是100vh
      animation.translateY(0 + 'vh').step()
    } else {
      animation.translateY(40 + 'vh').step()
    }
    that.setData({
      animationAddressMenu: animation.export(),
      addressMenuIsShow: isShow,
    })
  },
  // 点击地区选择取消按钮
  cityCancel: function (e) {
    this.startAddressAnimation(false)
  },
  // 点击地区选择确定按钮
  citySure: function (e) {
    var that = this
    var city = that.data.city
    var value = that.data.value
    that.startAddressAnimation(false)
    // 将选择的城市信息显示到输入框
    var areaInfo = that.data.provinces[value[0]].name + that.data.citys[value[1]].name + that.data.areas[value[2]].name
    that.setData({
      areaInfo: areaInfo,
    })
  },

  // 点击蒙版时取消组件的显示
  hideCitySelected: function (e) {
    console.log(e)
    this.startAddressAnimation(false)
  },
  // 处理省市县联动逻辑
  cityChange: function (e) {
    console.log(e)
    var value = e.detail.value
    var provinces = this.data.provinces
    var citys = this.data.citys
    var areas = this.data.areas
    var provinceNum = value[0]
    var cityNum = value[1]
    var countyNum = value[2]
    // 如果省份选择项和之前不一样，表示滑动了省份，此时市默认是省的第一组数据，
    if (this.data.value[0] != provinceNum) {
      var id = provinces[provinceNum].id
      this.setData({
        value: [provinceNum, 0, 0],
        citys: address.citys[id],
        areas: address.areas[address.citys[id][0].id],
      })
    } else if (this.data.value[1] != cityNum) {
      // 滑动选择了第二项数据，即市，此时区显示省市对应的第一组数据
      var id = citys[cityNum].id
      this.setData({
        value: [provinceNum, cityNum, 0],
        areas: address.areas[citys[cityNum].id],
      })
    } else {
      // 滑动选择了区
      this.setData({
        value: [provinceNum, cityNum, countyNum]
      })
    }
    console.log(this.data)
  },


  /**
 * 生命周期函数--监听页面加载
 */
  onLoad: function (options) {
    this.setData({
      address_id:options.id,
      userName: options.name,
      userPhone: options.phone,
      addressDetail: options.address,
    })
    // 默认联动显示北京
    var id = address.provinces[0].id
    this.setData({
      provinces: address.provinces,
      citys: address.citys[id],
      areas: address.areas[address.citys[id][0].id],
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})