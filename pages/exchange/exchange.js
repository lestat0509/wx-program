// pages/exchange/exchange.js
import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    images: [
    ],
    id: 0,
    awardPic: '',
    name: '',
    starNum: 0,
    duihuanNum: 0,
    barcodeNum: 0,
    currentTab: 0,
    goodsInfoItems: [
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      },
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      },
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      }
    ],
    dongtaiItems: [],
    required_number: 0,
    is_online: '',
    period: '',
    online_time: '',
    resttime: '',
    shuxingText: '',
    skuId: -1,
    selectShuxing: 0,
    shuxing1: '',
    shuxing2: '',
    shuxing3: '',
    skuList: [],
    shuxingList: [],
    pageNum:1,
    per_page:30,
    recommended:1,
    category_id:0,
    is_online:0,
    exchange_num:'reverse',
    created:'reverse',
    categoris: [],
    showOtherInfo: false,
    selectColor: 0,
    selectSize: 0,
    colorList: ['黑色', '白色', '灰色', '黑色', '白色', '灰色'],
    sizeList: ['超大', '大', '小', '超大', '大', '小'],
    goodsYunfei: '运费：1个娃娃（偏远地区加1个娃娃）',
    goodsRight: '>',
    goodsColor: '选择：颜色尺寸',
    content: '',
    goodsList: [],
    selectTop:0,
    goodsTopList: ['综合', '销量', '新品', '线上直邮', '门店自取'],
    selectKind:0,
    kindList: ['热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门', '热门']
  },
  gotoGoodsDetail: function (e) {
    let id = e.currentTarget.dataset['id']
    wx.navigateTo({
      url: '../awardDetail/awardDetail?scene=' + id,
    })
  },

  getCategoris: function () {
    let that = this
    network.get({
      url: 'categories'
    })
      .then(({ code, data }) => {
        let list = []
        let remen = {
          id: 0,
          name: '热门'
        }
        list.push(remen)
        if(data!= null &&data.length>0){
          list = list.concat(data)
        }
        
        this.setData({
          categoris: list
        })
        
      })
  },

  showChooseColor: function (e) {
    let id = e.currentTarget.dataset['id']
    let that = this
    if (wx.getStorageSync('branch')) {
      var branch_id = wx.getStorageSync('branch')
    } else {
      var branch_id = 1
    }
    network.get({
      url: 'branches/' + branch_id + '/goods/' + id
    })
      .then(({ code, data }) => {
        console.log(data)
        if (code == 0) {
          if (data != null) {
            that.setData({
              id:id,
              images: data.images,
              name: data.name,
              starNum: data.exchange_price,
              duihuanNum: data.total_num,
              content: data.detail,
              barcodeNum: data.barcode,
              is_online: data.is_online,
              online_time: data.online_time,
              period: data.period,
              required_number: data.required_number,
            })
            
            if (data.sku_attrs && data.sku_attrs.length > 0) {
              that.setData({
                shuxingList: data.sku_attrs,
                shuxing1: data.sku_attrs[0].value_list[0].v_name
              })
              if (data.sku_attrs.length > 1) {
                that.setData({
                  shuxing2: data.sku_attrs[1].value_list[0].v_name
                })
              }
              if (data.sku_attrs.length > 2) {
                that.setData({
                  shuxing2: data.sku_attrs[2].value_list[0].v_name
                })
              }
              // var shuxing = this.data.shuxing1
              // if (this.data.shuxing2.length > 0) {
              //   shuxing = shuxing + '-' + this.data.shuxing2
              // }
              // if (this.data.shuxing3.length > 0) {
              //   shuxing = shuxing + '-' + this.data.shuxing3
              // }
              // this.setData({
              //   shuxingText: shuxing
              // })


            }
            if (data.sku && data.sku.length > 0) {
              that.setData({
                skuList: data.sku,
                skuId: data.sku[0].sku_id
              })
            }
            if (data.fee_template && data.fee_template != null) {
              that.setData({
                baseyunfei: data.fee_template.base_fee,
                addyunfei: data.fee_template.add_fee
              })
            }
            this.setData({
              showOtherInfo: true
            })
            
          }
          
        }
      })
    
  },
  closeChooseColor: function () {
    this.setData({
      showOtherInfo: false,
    })
  },

  gotoDuihuan: function () {
    if (this.data.skuList && this.data.skuList.length > 0) {
      if (this.data.shuxingText.length == 0) {
        wx.showToast({
          title: '请选择商品颜色尺寸',
          icon: 'none',
          duration: 2000
        })
        return
      }
      var shuxing = this.data.shuxing1
      if (this.data.shuxing2.length > 0) {
        shuxing = shuxing + '-' + this.data.shuxing2
      }
      if (this.data.shuxing3.length > 0) {
        shuxing = shuxing + '-' + this.data.shuxing3
      }
      this.setData({
        shuxingText: shuxing
      })

      var skuId = -1;
      for (var i = 0; i < this.data.skuList.length; i++) {
        if (shuxing === this.data.skuList[i].suk_name) {
          skuId = this.data.skuList[i].sku_id
        }
      }
      if (skuId == -1) {
        wx.showToast({
          title: shuxing + '没有库存了哟',
          icon: 'none',
          duration: 2000
        })
        return
      }
    }
    wx.navigateTo({
      url: '../duihuanDetail/duihuanDetail?id=' + this.data.id + "&name=" + this.data.name + "&pic=" + this.data.images[0].url + "&price=" + this.data.starNum + "&is_online=" + this.data.is_online + "&color=" + this.data.color + "&size=" + this.data.size + "&skuId=" + this.data.skuId + '&skuname=' + this.data.shuxingText,
    })
    this.setData({
      showOtherInfo: false,
    })
  },
  sureChooseColor: function () {
    var shuxing = this.data.shuxing1
    if (this.data.shuxing2.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing2
    }
    if (this.data.shuxing3.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing3
    }
    this.setData({
      shuxingText: shuxing
    })
    console.log(shuxing)
    var skuId = -1;
    for (var i = 0; i < this.data.skuList.length; i++) {
      if (shuxing === this.data.skuList[i].suk_name) {
        skuId = this.data.skuList[i].sku_id
      }
    }
    if (skuId == -1) {
      wx.showToast({
        title: shuxing + '没有库存了哟',
        icon: 'none',
        duration: 2000
      })
    } else {
      this.setData({
        skuId: skuId
      })
      this.gotoDuihuan()
    }

  },
  chooseColor: function (e) {
    var index = e.currentTarget.dataset['index']
    this.setData({
      selectColor: index,
      shuxing1: this.data.shuxingList[0].value_list[index].v_name,
    })
    this.checkShuxing()
  },
  chooseSize: function (e) {
    var index = e.currentTarget.dataset['index']
    this.setData({
      selectSize: index,
      shuxing2: this.data.shuxingList[1].value_list[index].v_name,
    })
    console.log(index)
    this.checkShuxing()
  },

  checkShuxing: function () {
    var shuxing = this.data.shuxing1
    if (this.data.shuxing2.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing2
    }
    if (this.data.shuxing3.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing3
    }
    this.setData({
      shuxingText: shuxing
    })
    console.log(shuxing)
    var skuId = -1;
    for (var i = 0; i < this.data.skuList.length; i++) {
      if (shuxing === this.data.skuList[i].suk_name) {
        skuId = this.data.skuList[i].sku_id
      }
    }
    if (skuId == -1) {
      wx.showToast({
        title: shuxing + '没有库存了哟',
        icon: 'none',
        duration: 2000
      })
    } else {
      this.setData({
        skuId: skuId
      })
    }

  },
  gotoSearch:function(){
    wx.navigateTo({
      url: '../search/search',
    })
  },
  chooseTop:function(e){
    var index = e.currentTarget.dataset['index']
    this.setData({
      selectTop: index,
    })
    this.getGoodsList()
  },
  chooseKind:function(e){
    var index = e.currentTarget.dataset['index']
    var id = e.currentTarget.dataset['id']
    this.setData({
      selectKind: index,
      category_id:id
    })
    if(index == 0){
      this.setData({
        recommended: 1
      })
    }else{
      this.setData({
        recommended: 0
      })
    }
    this.getGoodsList()
  },


  getGoodsList:function(){
    let parmas = {}
    
    if(this.data.selectTop == 0){
      parmas = {
        pagination: { // 分页参数 如果不传返回全部
          page: this.data.pageNum, // 当前页
          per_page: this.data.per_page // 每页条数 默认10
        },
        filter: {
          recommended: this.data.recommended,
          category_id:this.data.category_id,

        } 
      }
    } else if (this.data.selectTop == 1){
      parmas = {
        pagination: { // 分页参数 如果不传返回全部
          page: this.data.pageNum, // 当前页
          per_page: this.data.per_page // 每页条数 默认10
        },
        filter: {
          recommended: this.data.recommended,
          category_id: this.data.category_id,

        },
        sort:{
          field:'exchange_num',
          reverse:true,
        }
      }
    } else if (this.data.selectTop == 2){
      parmas = {
        pagination: { // 分页参数 如果不传返回全部
          page: this.data.pageNum, // 当前页
          per_page: this.data.per_page // 每页条数 默认10
        },
        filter: {
          recommended: this.data.recommended,
          category_id: this.data.category_id,

        },
        sort: {
          field: 'created',
          reverse: true,
        }
      }
    } else if (this.data.selectTop == 3) {
      parmas = {
        pagination: { // 分页参数 如果不传返回全部
          page: this.data.pageNum, // 当前页
          per_page: this.data.per_page, // 每页条数 默认10
          
        },
        filter: {
          recommended: this.data.recommended,
          category_id: this.data.category_id,
          is_online: 1
        }
      }
    }else{
      parmas = {
        pagination: { // 分页参数 如果不传返回全部
          page: this.data.pageNum, // 当前页
          per_page: this.data.per_page, // 每页条数 默认10
          
        },
        filter: {
          recommended: this.data.recommended,
          category_id: this.data.category_id,
          is_online: 0
        }
      }
    }
    let that = this
    if (wx.getStorageSync('branch')) {
      var branch_id = wx.getStorageSync('branch')
    } else {
      var branch_id = 1
    }
    network.get({
      url: 'branches/' + branch_id + '/goods', data: parmas
    })
      .then(({ code, data }) => {
        
        if (code == 0) {
          if(data.list){
            that.setData({
              goodsList: data.list
            })
          }else{
            that.setData({
              goodsList:[]
            })
          }
          
        } else {
          that.setData({
            goodsList: []
          })
        }
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!app.globalData.categoris || app.globalData.categoris.length == 0) {
      this.getCategoris()
    } else {
      let list = []
      let remen = {
        id: 0,
        name: '热门'
      }
      list.push(remen)
      list = list.concat(app.globalData.categoris)
      
      this.setData({
        categoris: list
      })
    }
    this.getGoodsList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})