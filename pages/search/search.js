import network from '../../utils/network.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum:1,
    name:'蝙蝠娃娃',
    nameItems: [],
    barcode:'0000000000000',
    codeItems: [],
  },

  gotoGoodsDetail: function (e) {
    let id = e.currentTarget.dataset['id']
    wx.navigateTo({
      url: '../awardDetail/awardDetail?scene=' + id,
    })
  },
  // 监听输入
  watchContent: function (event) {
    this.setData({
      name: event.detail.value,
      barcode: event.detail.value
    })
    this.searchWawaName(1,this.data.name)
    this.searchWawaBarcode(1,this.data.barcode)
  },

  searchWawaName:function(pageNum,name){
    let that = this
    var branch_id = wx.getStorageSync('branch')
    network.get({
      url: 'branches/'+ branch_id +'/goods', data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 666 // 每页条数 默认10
        },
        filter: {
          name: name
        } // 过滤参数
      }
    })
      .then(({ code, data }) => {
        console.log("data")
        console.log(data)
        if (code == 0) {
          if (pageNum == 1 && data.list.length > 0) {
            that.setData({
              nameItems: data.list
            })
          } else {
            that.setData({
              nameItems: []
            })
          }
        } else {
          if (pageNum > 1) {
            that.setData({ pageNum: pageNum - 1 })
          }
        }

      })
  },

  searchWawaBarcode: function (pageNum,barcode) {
    let that = this
    var branch_id = wx.getStorageSync('branch')
    network.get({
      url: 'branches/' + branch_id + '/goods', data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 666 // 每页条数 默认10
        },
        filter: {
          barcode: barcode
        } // 过滤参数
      }
    })
      .then(({ code, data }) => {
        console.log("data")
        console.log(data)
        if (code == 0) {
          if (pageNum == 1 && data.list.length > 0) {
            that.setData({
              codeItems: data.list
            })
          } else {
            that.setData({
              codeItems: []
            })
          }
        } else {
          if (pageNum > 1) {
            that.setData({ pageNum: pageNum - 1 })
          }
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.searchWawaName(1,this.data.name),
    this.searchWawaBarcode(1,this.data.barcode)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})