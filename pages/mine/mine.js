import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headUrl:'../image/six_wawa.png',
    userName:'BatDoll',
    userId:'',
    userWawa:0
  },
  gotoExchange:function(){
    console.log('gooood')
    wx.navigateTo({
      url: '../record/record',
    })
  },
  gotoKefu:function(){
    wx.navigateTo({
      url: '../kefu/kefu',
    })
  },
  gotoUseRecord:function(){
    wx.navigateTo({
      url: '../userRecord/userRecord',
    })
  },

  gotoUseMoneyRecord: function () {
    wx.navigateTo({
      url: '../userMoneyRecord/userMoneyRecord',
    })
  },
  gotoSupprtData:function(){
    wx.navigateTo({
      url: '../supportRecord/supportRecord',
    })
  },
  gotoExplain:function(){
    wx.navigateTo({
      url: '../explain/explain',
    })
  },
  gotoAddress: function () {
    var id=this.data.userId
    wx.navigateTo({
      url: '../address/address?userId=' + id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.globalData.userInfo!= null){
        this.setData({
          headUrl: app.globalData.userInfo.avatar,
          userName: app.globalData.userInfo.nickname,
          userId: app.globalData.userInfo.uid,
          userWawa: app.globalData.userInfo.doll
        })
    }else{
      if (!app.userCallBack) {
        app.userCallBack = (data) => {
          if (app.globalData.userInfo != null) {
            this.setData({
              headUrl: app.globalData.userInfo.avatar,
              userName: app.globalData.userInfo.nickname,
              userId: app.globalData.userInfo.uid,
              userWawa: app.globalData.userInfo.doll
            })
          }
        }
      }
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    network.get({
      url: 'auth/me'
    })
      .then(({ code, data }) => {
        console.log("data")
        console.log(data)
        if (code == 0) {
          if(data != null){
            app.globalData.userInfo=data
            that.setData({
              headUrl: app.globalData.userInfo.avatar,
              userName: app.globalData.userInfo.nickname,
              userId: app.globalData.userInfo.uid,
              userWawa: app.globalData.userInfo.doll
            })
          }

        }

      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})