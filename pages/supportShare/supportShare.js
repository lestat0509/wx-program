import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shareUid:'',
    goods_url:'',
    mineDoll:0,
    shareName:'',
    shareDoll:0,
    supportDoll:0,

  },
  gotoHome:function(){
    wx.redirectTo({
      url: '../index/index'
    })
  },

  bindGetUserInfo: function (e) {
    var that = this
    if (app.globalData.userInfo!= null){
      return
    }
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.code) {
          let code = res.code
          wx.getUserInfo({
            success: function (res) {
              network.post({
                url: 'auth/login', data: {
                  code: code,
                  encryptedData: res.encryptedData,
                  iv: res.iv
                }
              })
                .then(({ code, data }) => {
                  if (code == 0) {
                    app.globalData.userInfo = data.userInfo
                    app.globalData.token = data.token
                    wx.setStorageSync('token', data.token)
                    that.setData({
                      mineDoll: app.globalData.userInfo.doll
                    })
                  }

                })
            }
          })

        }
      }
    })

  },
  numInput:function(e){
    this.setData({
      supportDoll: e.detail.value
    })
  },

  support:function(){
    let that = this
    network.post({
      url: 'sponsor', data: {
        to_uid: that.data.shareUid,
        doll: that.data.supportDoll 
      },
    })
      .then(({ code, data }) => {

        if (code == 0) {
          wx.showToast({ title: '赞助成功', icon: 'none' })
        } else if(code==-10012){
          wx.showToast({ title: '娃娃不足', icon: 'none' })
        } else {
          console.log(code)
          wx.showToast({ title: '系统错误', icon: 'none' })
        }
      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    this.setData({
      shareUid: options.uid,
      goods_url: options.url,
      shareName: options.name,
      shareDoll: options.doll,
    })
    wx.getSetting({
      success: function (res) {

        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          // // 登录
          
          if (app.globalData.userInfo != null) {
            that.setData({
              mineDoll: app.globalData.userInfo.doll
            })
          } else {
            
            if (!app.userCallBack) {
              app.userCallBack = (data) => {
                that.setData({
                  mineDoll: app.globalData.userInfo.doll
                })
              }
            }
          }

        } else {
          
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})