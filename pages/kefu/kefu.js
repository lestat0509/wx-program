Page({

  /**
   * 页面的初始数据
   */
  data: {
    showBig:false,
    showSave:false
  },

  bindTouchStart: function (e) {
    this.startTime = e.timeStamp;
  },
bindTouchEnd: function (e) {
    this.endTime = e.timeStamp;
  },

  saveLocal:function(){
    let that = this
    wx.downloadFile({
      url: 'https://static.bianfuyule.com/gzh.png',
      success: function (res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function (res) {
            wx.showToast({ title: '保存成功', icon: 'none' })
            that.setData({
              showSave: false
            })
          },
          fail: function (res) {
            wx.showToast({ title: '保存失败', icon: 'none' })
          }
        })
      },
      fail: function () {
        wx.showToast({ title: '保存失败', icon: 'none' })
      }
    })
  },

  savePic:function(){
    this.setData({
      showSave:true
    })
  },

  showbigPic:function(){
      this.setData({
        showBig:true
      })
  },

  closeBig:function(){
    if (this.endTime - this.startTime < 350) {
      this.setData({
        showBig: false,
        showSave: false
      })
    }
    
  },
  copyNum:function(){
    let that = this
    wx.setClipboardData({
      data: '82038940',
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showModal({
              title: '提示',
              content: '复制成功',
              showCancel: false

            })

          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})