import network from '../../utils/network.js'
import util from '../../utils/util.js'
const app = getApp()
let Wxparse = require("../../wxParse/wxParse.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    painyuanText:'',
    baseyunfei:0,
    addyunfei:0,
    shuxingText:'',
    skuId:-1,
    selectShuxing:0,
    shuxing1:'',
    shuxing2:'',
    shuxing3:'',
    skuList:[],
    shuxingList:[],
    wawaTags:[],
    color:'',
    size:'',
    showYoufeiInfo:false,
    showOtherInfo:false,
    selectColor:0,
    selectSize:0,
    colorList: ['黑色', '白色', '灰色', '黑色', '白色', '灰色'],
    sizeList: ['超大', '大', '小', '超大', '大', '小'],
    goodsYunfei:'运费：1个娃娃（偏远地区加1个娃娃）',
    goodsRight:'>',
    goodsColor:'选择：颜色尺寸',
    content: '',
    pageNum:1,
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    images:[    
    ],
    id:0,
    awardPic:'',
    name:'',
    starNum:0,
    duihuanNum:0,
    barcodeNum:0,
    currentTab:0,
    goodsInfoItems:[
      {
        img:'../image/share_top.png',
        content:'质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      },
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      },
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      }
    ],
    dongtaiItems: [],
    required_number:0,
    is_online:'',
    period:'',
    online_time:'',
    resttime:''
  },
  gotoHome:function(){
    wx.switchTab({

      url: '../index/index',

    });

  },
  closeYunfei:function(){
    this.setData({
      showYoufeiInfo: false
    })
  },
  showYunfei: function () {
    this.setData({
      showYoufeiInfo: true
    })
  },
  showChooseColor:function(){
    var shuxing = this.data.shuxing1
    if (this.data.shuxing2.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing2
    }
    if (this.data.shuxing3.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing3
    }
    this.setData({
      shuxingText: shuxing
    })
      this.setData({
        showOtherInfo: true
      })
  },
  closeChooseColor:function(){
    this.setData({
      showOtherInfo: false,
    })
  },
  sureChooseColor:function(){
    var shuxing = this.data.shuxing1
    if (this.data.shuxing2.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing2
    }
    if (this.data.shuxing3.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing3
    }
    this.setData({
      shuxingText: shuxing
    })
    console.log(shuxing)
    var skuId = -1;
    for (var i = 0; i < this.data.skuList.length; i++) {
      if (shuxing === this.data.skuList[i].suk_name) {
        skuId = this.data.skuList[i].sku_id
      }
    }
    if (skuId == -1) {
      wx.showToast({
        title:  shuxing + '没有库存了哟',
        icon: 'none',
        duration: 2000
      })
    } else {
      this.setData({
        skuId: skuId
      })
      this.setData({
        showOtherInfo: false,
      })
    }
    
  },
  chooseShuxingThree:function(){
    var index = e.currentTarget.dataset['index']
    this.setData({
      selectShuxing: index,
      shuxing3: this.data.shuxingList[2].value_list[index].v_name,
    })
    this.checkShuxing()
  },
  chooseColor:function(e){
    var index = e.currentTarget.dataset['index']
    this.setData({
      selectColor: index,
      shuxing1: this.data.shuxingList[0].value_list[index].v_name,
    })
    this.checkShuxing()
  },
  chooseSize: function (e) {
    var index = e.currentTarget.dataset['index']
    this.setData({
      selectSize: index,
      shuxing2: this.data.shuxingList[1].value_list[index].v_name,
    })
    console.log(index)
    this.checkShuxing()
  },

  checkShuxing:function(){
    var shuxing = this.data.shuxing1
    if(this.data.shuxing2.length>0){
      shuxing = shuxing + '-' + this.data.shuxing2
    }
    if (this.data.shuxing3.length > 0) {
      shuxing = shuxing + '-' + this.data.shuxing3
    }
    this.setData({
      shuxingText: shuxing
    })
    console.log(shuxing)
    var skuId = -1;
    for(var i = 0;i < this.data.skuList.length;i++){
      if (shuxing === this.data.skuList[i].suk_name){
        skuId = this.data.skuList[i].sku_id
      }
    }
    if(skuId == -1){
      wx.showToast({
        title: shuxing + '没有库存了哟',
        icon:'none',
        duration: 2000
      })
    }else{
      this.setData({
        skuId:skuId
      })
    }

  },
  getPianyuanArea:function(){
    let that = this
    network.get({
      url: 'remote_areas'
    })
      .then(({ code, data }) => {
        console.log(data)
        if (code == 0) {
          if (data != null) {
            that.setData({
              painyuanText:data
            })
          }
        }
      })
  },
  gotoDuihuan:function(){
    if(this.data.skuList&& this.data.skuList.length>0){
      if(this.data.shuxingText.length==0){
        wx.showToast({
          title: '请选择商品颜色尺寸',
          icon: 'none',
          duration: 2000
        })
        return
      }
      var shuxing = this.data.shuxing1
      if (this.data.shuxing2.length > 0) {
        shuxing = shuxing + '-' + this.data.shuxing2
      }
      if (this.data.shuxing3.length > 0) {
        shuxing = shuxing + '-' + this.data.shuxing3
      }
      this.setData({
        shuxingText: shuxing
      })
      
      var skuId = -1;
      for (var i = 0; i < this.data.skuList.length; i++) {
        if (shuxing === this.data.skuList[i].suk_name) {
          skuId = this.data.skuList[i].sku_id
        }
      }
      if (skuId == -1) {
        wx.showToast({
          title: shuxing + '没有库存了哟',
          icon: 'none',
          duration: 2000
        })
        return
      } 
    }
    wx.navigateTo({
      url: '../duihuanDetail/duihuanDetail?id=' + this.data.id + "&name=" + this.data.name + "&pic=" + this.data.images[0].url + "&price=" + this.data.starNum + "&is_online=" + this.data.is_online + "&color=" + this.data.color + "&size=" + this.data.size + "&skuId=" + this.data.skuId + '&skuname=' + this.data.shuxingText + '&baseFee=' + this.data.baseyunfei + '&addFee=' + this.data.addyunfei,
    })
  },
  //滑动切换
  swiperTab: function (e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
    if(this.data.currentTab==0){
      this.setData({
        view: {
          Height: 2800
        }
      })
    }else{
      this.setData({
        view: {
          Height: 1000
        }
      })
    }
  },
  //点击切换
  clickTab: function (e) {

    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
    if (this.data.currentTab == 0) {
      // this.setData({
      //   view: {
      //     Height: 2800
      //   }
      // })
      // Wxparse.wxParse('article', 'html', that.data.content, that, 5);
    } else {
      this.setData({
        view: {
          Height: 1000
        }
      })
    }
  },

  getGoodsDetail:function(){
    let that = this
    if (wx.getStorageSync('branch')) {
      var branch_id = wx.getStorageSync('branch')
    } else {
      var branch_id = 1
    }
    network.get({
      url: 'branches/' + branch_id + '/goods/'+this.data.id
    })
      .then(({ code, data }) => {
        console.log(data)
        if (code == 0) {
          if (data != null) {
            console.log('11111111111')
            that.setData({
              images: data.images,
              name: data.name,
              starNum: data.exchange_price,
              duihuanNum: data.total_num,
              content: data.detail,
              barcodeNum: data.barcode,
              is_online: data.is_online,
              online_time: data.online_time,
              period:data.period,
              required_number: data.required_number,
            })
            if(data.tags){
              that.setData({
                wawaTags:data.tags
              })
            }
            if (data.sku_attrs && data.sku_attrs.length > 0){
              that.setData({
                shuxingList: data.sku_attrs,
                shuxing1: data.sku_attrs[0].value_list[0].v_name
              })
              if (data.sku_attrs.length>1){
                that.setData({
                  shuxing2: data.sku_attrs[1].value_list[0].v_name
                })
              }
              if (data.sku_attrs.length > 2) {
                that.setData({
                  shuxing2: data.sku_attrs[2].value_list[0].v_name
                })
              }
              // var shuxing = this.data.shuxing1
              // if (this.data.shuxing2.length > 0) {
              //   shuxing = shuxing + '-' + this.data.shuxing2
              // }
              // if (this.data.shuxing3.length > 0) {
              //   shuxing = shuxing + '-' + this.data.shuxing3
              // }
              // this.setData({
              //   shuxingText: shuxing
              // })
              

            }
            if (data.sku && data.sku.length > 0) {
              that.setData({
                skuList: data.sku,
                skuId: data.sku[0].sku_id
              })
            }
            if (data.fee_template&&data.fee_template!= null){
              that.setData({
                baseyunfei: data.fee_template.base_fee,
                addyunfei: data.fee_template.add_fee
              })
            }

            console.log('goodoododododo')
            Wxparse.wxParse('article', 'html', that.data.content, that, 5);

            console.log('87342642878429')
          }
          if (data.is_online == 2) {
            let star = data.online_time
            let per = data.period
            let timenow = util.formatTime(new Date());
            this.setData({
              resttime: util.getPeriod(star, per, timenow)
            })
          }           
        }
      })
  },

  getExChangeRecord:function(pageNum){
    let that = this
    network.get({
      url: 'orders/latest/'+this.data.id, data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 13 // 每页条数 默认10
        }

      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          for (var i = 0; i < data.list.length; i++) {
            var dongtaiTime = data.list[i].order_time.slice(5, 16);
            data.list[i].order_time = dongtaiTime
          }
          if (pageNum == 1) {
            that.setData({ dongtaiItems: data.list })
          } else {
            that.setData({ dongtaiItems: that.data.dongtaiItems.concat(data.list) })
          }
        } else {
          if (pageNum > 1) {
            that.setData({ pageNum: that.data.pageNum - 1 })
          }
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      view: {
        Height: 1500
      },
      id:options.scene
    })
    this.getGoodsDetail()
    this.getExChangeRecord(1)
    this.getPianyuanArea()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {   

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getExChangeRecord(1)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
    this.getExChangeRecord(this.data.pageNum)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})