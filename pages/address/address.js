import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum: 1,
    id:'',
    addressItems: [],
    txtStyle:'',
    delBtnWidth: 180
  },

  getAdressList: function() {
    let that = this
    var userId=this.data.id
    network.get({
      url: 'addresses/' + userId , data: {
        pagination: { // 分页参数 如果不传返回全部
          page: 1, // 当前页
          per_page: 10 // 每页条数 默认10
        },
      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          if (data.data && data.data.length > 0) {
            that.setData({ addressItems: data.data })
          } else {
            that.setData({
              addressItems: this.data.addressItems
            })
          } 
        }
      })
  },
  goAddress: function (e) {
    var id = e.currentTarget.dataset['id'];
    let areaInfo = e.currentTarget.dataset['address']
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];   //当前页面
    var prevPage = pages[pages.length - 2];  //上一个页面
    prevPage.setData({
      areaInfo: areaInfo,
      address_id:id,    //给上级页面的变量赋值
    }) 
    wx.navigateBack()   //返回上级页面
  }, 

  goEdit: function(e){
    var id = e.currentTarget.dataset.id;
    var name = e.currentTarget.dataset.name;
    var phone = e.currentTarget.dataset.phone;
    var address = e.currentTarget.dataset.address;
    wx.navigateTo({
      url: '../editAddress/editAddress?id=' + id + '&name=' + name + '&phone=' + phone + '&address=' + address,
    })
  },
  
  goDele: function (e) {
    let that = this
    var id = e.currentTarget.dataset['id'];
    var index = e.currentTarget.dataset['index'];
    network.delete({
      url: 'address/' + id,
    })
      .then(({ code, data }) => {
        if (code == 0) {
          this.data.addressItems.splice(index, 1);
          wx.showToast({
            title: '删除成功',
            icon: 'success',
          })
          this.setData({
            addressItems: this.data.addressItems
          })
        } else {
          console.log(code)
          wx.showToast({ title: '系统错误', icon: 'none' })
        }
      })
  },


  addAddress: function(e){
    var userId=this.data.id
    wx.navigateTo({
      url: '../addAddress/addAddress',
    })
  },

  touchS: function (e) {
    if (e.touches.length == 1) {
      this.setData({
        //设置触摸起始点水平方向位置
        startX: e.touches[0].clientX
      });
    }
  },

  touchM: function (e) {
    if (e.touches.length == 1) {
      //手指移动时水平方向位置
      var moveX = e.touches[0].clientX;
      //手指起始点位置与移动期间的差值
      var disX = this.data.startX - moveX;
      var delBtnWidth = this.data.delBtnWidth;
      var txtStyle = "";
      if (disX == 0 || disX < 0) {//如果移动距离小于等于0，文本层位置不变
        txtStyle = "left:0rpx";
      } else if (disX > 0) {//移动距离大于0，文本层left值等于手指移动距离
        txtStyle = "left:-" + disX + "rpx";
        if (disX >= delBtnWidth) {
          //控制手指移动距离最大值为删除按钮的宽度
          txtStyle = "left:-" + delBtnWidth + "rpx";
        }
      }
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.addressItems;
      list[index]['txtStyle'] = txtStyle;
      //更新列表的状态
      this.setData({
        addressItems: list
      });
    }
  },
  touchE: function (e) {
    if (e.changedTouches.length == 1) {
      //手指移动结束后水平位置
      var endX = e.changedTouches[0].clientX;
      //触摸开始与结束，手指移动的距离
      var disX = this.data.startX - endX;
      var delBtnWidth = this.data.delBtnWidth;
      //如果距离小于删除按钮的1/2，不显示删除按钮
      var txtStyle = disX > delBtnWidth / 2 ? "left:-" + delBtnWidth + "rpx" : "left:0rpx";
      //获取手指触摸的是哪一项
      var index = e.currentTarget.dataset.index;
      var list = this.data.addressItems;
      var del_index = '';
      disX > delBtnWidth / 2 ? del_index = index : del_index = '';
      list[index].txtStyle = txtStyle;
      //更新列表的状态
      this.setData({
        addressItems: list,
        del_index: del_index
      });
    }
  },


  /**
 * 生命周期函数--监听页面加载
 */
  onLoad: function (options) {
    this.setData({
      id: options.userId,
    })
    this.getAdressList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getAdressList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getAdressList()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
    this.getAdressList(this.data.pageNum)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})