import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageNum:1,
    dongtaiItems:[
      {
        name:"李雷",
        wawa:"小猪佩琦",
        time:"20分钟前"
      },
    ]
  },

  getDynamic:function(pageNum){
    let that = this
    network.get({
      url: 'orders/latest', data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 13 // 每页条数 默认10
        }

      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          for(var i = 0; i < data.list.length; i++)
    {
      var dongtaiTime = data.list[i].order_time.slice(5, 16);
      data.list[i].order_time = dongtaiTime
    }
          if (pageNum == 1) {
            that.setData({ dongtaiItems: data.list })
          } else {
            that.setData({ dongtaiItems: that.data.dongtaiItems.concat(data.list) })
          }
        } else {
          if (pageNum > 1) {
            that.setData({ pageNum: that.data.pageNum - 1 })
          }
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getDynamic(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getDynamic(1)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
    this.getDynamic(this.data.pageNum)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})