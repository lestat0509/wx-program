import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    baseyunfei: 0,
    addyunfei: 0,
    pianyuanList:[],
    painyuanText:'',
    skuName:'',
    skuId:-1,
    color:'',
    size:'',
    id:0,
    areaAddress: '',
    animationAddressMenu: {},
    animationAddressMenuMendian: {},
    addressMenuIsShow: false,
    value: [0, 0, 0],
    areaInfo: '请选择收货地址',
    address_id:'0',
    goodsImg:'',
    goodsName:'',
    goodsStarNum:0,
    showSuc:false,
    showFail:false,
    userNote:'',
    ajxtrue:false,
    num: 1,
    minusStatus: 'disabled',
    is_online:0
  },
  getPianyuanArea: function () {
    let that = this
    network.get({
      url: 'remote_areas'
    })
      .then(({ code, data }) => {
        console.log(data)
        if (code == 0) {
          if (data != null) {
            that.setData({
              painyuanText: data
            })
            that.setData({
              pianyuanList: data.split(',')
            })
            console.log('goooooooood')
            console.log(that.data.pianyuanList)
          }
        }
      })
  },
  closeFail:function(){
    this.setData({
      showFail:false
    })
  },
  closeSuc: function () {
    this.setData({
      showSuc: false
    })
  },
  getSupport:function(){
    var scene = "?url=" + this.data.goodsImg + "&uid=" + app.globalData.userInfo.id + "&name=" + app.globalData.userInfo.nickname + "&doll=" + app.globalData.userInfo.doll
    wx.navigateTo({
      url: '../supportShare/supportShare' + scene,
    })
  },

  chooseWay:function(e){
    var that = this
    var animation = wx.createAnimation({
      duration: 100,
      timingFunction: 'ease',
    })
    var isShow = false
    if (isShow) {
      // vh是用来表示尺寸的单位，高度全屏是100vh
      animation.translateY(0 + 'vh').step()
    } else {
      animation.translateY(40 + 'vh').step()
    }
     
    if (this.data.currentTab == 0) {
      that.setData({
        animationAddressMenu: animation.export(),
        addressMenuIsShow: isShow,
      })
    } else {
      that.setData({
        animationAddressMenuMendian: animation.export(),
        mendianIsShow: isShow,
      })
    }
    var index = e.currentTarget.dataset['index'];
    this.setData({
      currentTab:index,
      areaAddress: wx.getStorageSync('branch_name')
    })   
  },
  //点击跳转地址管理
  myaddress: function () {
    var userId = wx.getStorageSync('userId')
    wx.navigateTo({
      url: '../address/address?userId=' + userId,
    })
  },
  
  userNoteInput: function (e) {
    this.setData({
      userNote: e.detail.value
    })
  },

  gotoExchange:function(){
    let that = this
    if (wx.getStorageSync('branch')) {
      var branch_id = wx.getStorageSync('branch')
    } else {
      var branch_id = 1
    }
    var isAddYunfee = false
    if (parseInt(this.data.baseyunfei) +parseInt(this.data.addyunfei)!=0){
      for (var i = 0;i< this.data.pianyuanList.length;i++){
        if (this.data.areaInfo.indexOf(this.data.pianyuanList[i])==0){
          isAddYunfee = true
        }
      }
    }
    var yunfee = this.data.baseyunfei
    if(isAddYunfee){
      yunfee = parseInt(this.data.baseyunfei) + parseInt(this.data.addyunfei)
    }
    if(this.data.is_online ==1){
      wx.showModal({
        content: '运费'+yunfee + '个娃娃，'+'请核对订单，确定后无法取消',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            network.post({
              url: 'branches/' + branch_id + '/orders', data: {
                goods_id: that.data.id, // 商品id,必填
                exchange_type: that.data.is_online, // 兑换类型 1：线上 0： 线下 必填
                amount: that.data.num,
                address_id: that.data.address_id,
                remark: that.data.userNote,
                sku_id: that.data.skuId
              },
            })
              .then(({ code, data }) => {
                if (code == 0) {
                  that.showSucArea()
                } else if (code == -10012) {
                  that.showFailArea()
                } else if (code == -10023) {
                  wx.showToast({ title: '该兑换品此门店暂停兑换，请尝试选择其他门店进行兑换', icon: 'none' })
                } else if (code == -10022) {
                  wx.showToast({ title: '你的兑换太快了，请休息一下稍后再试', icon: 'none' })
                } else if (code == -10025) {
                  wx.showToast({ title: '库存不足', icon: 'none' })
                } else if (code == -10010) {
                  wx.showToast({ title: '请选择收货地址', icon: 'none' })
                } else if (code == -10026) {
                  wx.showToast({ title: '请首页选择门店', icon: 'none' })
                }
                else {
                  console.log(code)
                  wx.showToast({ title: '系统错误', icon: 'none' })
                }
                that.getUser()
              })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })   
    }else{
      wx.showModal({
        content: '请核对订单，确定后无法取消',
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
            network.post({
              url: 'branches/' + branch_id + '/orders', data: {
                goods_id: that.data.id, // 商品id,必填
                exchange_type: that.data.is_online, // 兑换类型 1：线上 0： 线下 必填
                amount: that.data.num,
                address_id: that.data.address_id,
                remark: that.data.userNote,
                sku_id: that.data.skuId
              },
            })
              .then(({ code, data }) => {
                if (code == 0) {
                  that.showSucArea()
                } else if (code == -10012) {
                  that.showFailArea()
                } else if (code == -10023) {
                  wx.showToast({ title: '该兑换品此门店暂停兑换，请尝试选择其他门店进行兑换', icon: 'none' })
                } else if (code == -10022) {
                  wx.showToast({ title: '你的兑换太快了，请休息一下稍后再试', icon: 'none' })
                } else if (code == -10025) {
                  wx.showToast({ title: '库存不足', icon: 'none' })
                } else if (code == -10010) {
                  wx.showToast({ title: '请选择收货地址', icon: 'none' })
                } else if (code == -10026) {
                  wx.showToast({ title: '请首页选择门店', icon: 'none' })
                }
                else {
                  console.log(code)
                  wx.showToast({ title: '系统错误', icon: 'none' })
                }
                that.getUser()
              })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })   
    }
           
  },
  getUser:function(){
    let that = this
    network.get({
      url: 'auth/me'
    })
      .then(({ code, data }) => {
        console.log("data")
        console.log(data)
        if (code == 0) {
          if (data != null) {
            app.globalData.userInfo = data
          }
        }
      })
  },

  showFailArea: function () {
    this.setData({ showFail: true })
  },

  showSucArea: function () {
    this.setData({ showSuc: true })
  },

  /* 点击减号 */
  bindMinus: function () {
    var num = this.data.num;
    if (num > 1) {
      num--;
    }
    var minusStatus = num <= 1 ? 'disabled' : 'normal';
    this.setData({
      num: num,
      minusStatus: minusStatus
    });
  },
  /* 点击加号 */
  bindPlus: function () {
    var num = this.data.num;
    num++;
    var minusStatus = num < 1 ? 'disabled' : 'normal';  
    this.setData({
      num: num,
      minusStatus: minusStatus
    });
  },
  /* 输入框事件 */
  bindManual: function (e) {
    var num = e.detail.value; 
    this.setData({
      num: num
    });
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getPianyuanArea()
    this.setData({
      id: options.id,
      goodsImg: options.pic,
      goodsName: options.name,
      goodsStarNum: options.price,
      is_online: options.is_online,
      color: options.color ? options.color:'',
      size: options.size ? options.size : '',
      skuId: options.skuId ? options.skuId : -1,
      skuName: options.skuname ? options.skuname : '',
      baseyunfei: options.baseFee ? options.baseFee : 0,
      addyunfei: options.addFee ? options.addFee : 0
    })
    this.setData({
      areaAddress: wx.getStorageSync('branch_name'),
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var scene = "?url=" + this.data.goodsImg + "&uid=" + app.globalData.userInfo.id + "&name=" + app.globalData.userInfo.nickname + "&doll=" + app.globalData.userInfo.doll
    return {
      title: app.globalData.userInfo.nickname + "想要兑换娃娃，快来帮帮TA!",
      path: 'pages/index/index' + scene,
      imageUrl:'../image/fenxiang.png'
    }
  }
})