const version = '3.0.1'
const isTest = false
const appSetting = {
  version,
  appSecret: '1ac940bf03d2bd6a5b961786b973f9be',
  appId: 'wx43049fe3a91f1b5c',
  domain: isTest ? 'http://47.103.18.251:5000' :'https://api.bianfuyule.com',
  path: '/api/wxapp/'
}

module.exports = appSetting