import network from '../../utils/network.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    supportItems:[
      {
        name:'李雷',
        num:15,
        time:'2018-12-22 10:34:54'
      },
      {
        name: '李雷',
        num: 15,
        time: '2018-12-22 10:34:54'
      },
      {
        name: '李雷',
        num: 15,
        time: '2018-12-22 10:34:54'
      },
      {
        name: '李雷',
        num: 15,
        time: '2018-12-22 10:34:54'
      }
    ]
  },

  getZanzhuList: function (pageNum) {
    let that = this
    network.get({
      url: 'sponsor_records', data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 10 // 每页条数 默认10
        }

      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          if (pageNum == 1) {
            that.setData({ supportItems: data.list })
          } else {
            that.setData({
              supportItems: this.data.supportItems.concat(data.list)
            })

          }
        } else {
          if (pageNum > 1) {
            this.setData({ pageNum: this.data.pageNum - 1 })
          }
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getZanzhuList(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getZanzhuList(1)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
    this.getZanzhuList(this.data.pageNum)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})