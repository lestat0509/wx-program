import network from '../../utils/network.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:0,
    mode:"",
    name:'',
    adress:"",
    goodsImg:'',
    goodsName:'',
    goodsStarNum:0,
    status:'',
    orderNum:'',
    time:'',
    wuliuInfo:'',
    wuliuNum:'',
    mendian:''

  },
  copyText: function (event) {
    let that = this
    wx.setClipboardData({
      data: event.currentTarget.dataset.code,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showModal({
              title: '提示',
              content: '复制成功',
              showCancel: false

            })
            
          }
        })
      }
    })
  },

  getOrderDetail: function () {
    let that = this
    network.get({
      url: 'orders/' + this.data.id


    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          if (data != null) {
            that.setData({
              mode: data.exchange_type==0?'门店自取':'线上直邮',
              name: data.address.name,
              adress: data.address.address,
              goodsImg: data.pic_url,
              goodsName: data.goods_name,
              goodsStarNum: data.amount,
              status: data.status,
              orderNum: data.order_no,
              time: data.order_time,
              wuliuInfo: data.courier_company,
              wuliuNum: data.courier_no,
              mendian: data.branch_name
            })
          }
        } else {

        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      id:options.scene
    })
    this.getOrderDetail()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})