import network from '../../utils/network.js'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type:0,
    rank_type:0,
    currentTab: 0,
    allItems:[],
    rankingItems:[
    ]
  },
  //滑动切换
  swiperTab: function (e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
    console.log(this.data.currentTab)
    if (this.data.currentTab == 0){
      this.setData({
        type:0,
        rank_type:0,
      })
    } else if (this.data.currentTab == 1) {
      this.setData({
        type: 0,
        rank_type: 1,
      })
    } else if (this.data.currentTab == 2) {
      this.setData({
        type: 1,
        rank_type: 0,
      })
    } else {
      this.setData({
        type: 1,
        rank_type: 1,
      })
    }
    this.getRankingList()
  },
  //点击切换
  clickTab: function (e) {

    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      // this.setData({
      //   view: {
      //     Height: this.data.recommendItems.length / 2 * 300 + 20
      //   }
      // })
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
    if (this.data.currentTab == 0) {
      this.setData({
        type: 0,
        rank_type: 0,
      })
    } else if (this.data.currentTab == 1) {
      this.setData({
        type: 0,
        rank_type: 1,
      })
    } else if (this.data.currentTab == 2) {
      this.setData({
        type: 1,
        rank_type: 0,
      })
    } else {
      this.setData({
        type: 1,
        rank_type: 1,
      })
    }
    // this.getRankingList()
  },

  getRankingList: function (pageNum,) {
    

    let that = this
    network.get({
      url: 'ranking', data: {
        type: this.data.type, 	//  0:娃娃榜 1:兑换榜 不传默认为0
        rank_type: this.data.rank_type,  // 传type参数表示月榜 不传则为总榜
        top: 20	// 排行榜取前多少名 不传默认为10

      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          
          that.setData({ allItems: data,
            rankingItems: data})
          if (that.data.allItems != null && that.data.allItems.length > 3){
              that.setData({
                rankingItems: this.data.rankingItems.splice(3, this.data.rankingItems.length)
              })
          }else{
            that.setData({ rankingItems: [] })
          }
          
        } else {
          
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.getRankingList()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})