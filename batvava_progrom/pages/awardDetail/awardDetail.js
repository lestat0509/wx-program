import network from '../../utils/network.js'
import util from '../../utils/util.js'
const app = getApp()
let Wxparse = require("../../wxParse/wxParse.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    content: '',
    pageNum:1,
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    images:[    
    ],
    id:0,
    awardPic:'',
    name:'',
    starNum:0,
    duihuanNum:0,
    barcodeNum:0,
    currentTab:0,
    goodsInfoItems:[
      {
        img:'../image/share_top.png',
        content:'质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      },
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      },
      {
        img: '../image/share_top.png',
        content: '质量非常好，包装漂亮，物流迅速。做工精细 玩具好看,质量也好。买来可以拍下了咯。好好看,抱起来软软的。'
      }
    ],
    dongtaiItems: [],
    required_number:0,
    is_online:'',
    period:'',
    online_time:'',
    resttime:''
  },
  gotoDuihuan:function(){
    wx.navigateTo({
      url: '../duihuanDetail/duihuanDetail?id=' + this.data.id + "&name=" + this.data.name + "&pic=" + this.data.images[0].url + "&price=" + this.data.starNum + "&is_online=" + this.data.is_online,
    })
  },
  //滑动切换
  swiperTab: function (e) {
    var that = this;
    that.setData({
      currentTab: e.detail.current
    });
    if(this.data.currentTab==0){
      this.setData({
        view: {
          Height: 2800
        }
      })
    }else{
      this.setData({
        view: {
          Height: 1000
        }
      })
    }
  },
  //点击切换
  clickTab: function (e) {

    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
    if (this.data.currentTab == 0) {
      // this.setData({
      //   view: {
      //     Height: 2800
      //   }
      // })
      // Wxparse.wxParse('article', 'html', that.data.content, that, 5);
    } else {
      this.setData({
        view: {
          Height: 1000
        }
      })
    }
  },

  getGoodsDetail:function(){
    let that = this
    network.get({
      url: 'goods/'+this.data.id
    })
      .then(({ code, data }) => {
        console.log(data)
        if (code == 0) {
          if (data != null) {
            console.log('11111111111')
            that.setData({
              images: data.images,
              name: data.name,
              starNum: data.exchange_price,
              duihuanNum: data.total_num,
              content: data.detail,
              barcodeNum: data.barcode,
              is_online: data.is_online,
              online_time: data.online_time,
              period:data.period,
              required_number: data.required_number,
            })
            console.log('goodoododododo')
            Wxparse.wxParse('article', 'html', that.data.content, that, 5);

            console.log('87342642878429')
          }
          if (data.is_online == 2) {
            let star = data.online_time
            let per = data.period
            let timenow = util.formatTime(new Date());
            this.setData({
              resttime: util.getPeriod(star, per, timenow)
            })
          }           
        }
      })
  },

  getExChangeRecord:function(pageNum){
    let that = this
    network.get({
      url: 'orders/latest/'+this.data.id, data: {
        pagination: { // 分页参数 如果不传返回全部
          page: pageNum, // 当前页
          per_page: 13 // 每页条数 默认10
        }

      }
    })
      .then(({ code, data }) => {

        console.log(data)
        if (code == 0) {
          for (var i = 0; i < data.list.length; i++) {
            var dongtaiTime = data.list[i].order_time.slice(5, 16);
            data.list[i].order_time = dongtaiTime
          }
          if (pageNum == 1) {
            that.setData({ dongtaiItems: data.list })
          } else {
            that.setData({ dongtaiItems: that.data.dongtaiItems.concat(data.list) })
          }
        } else {
          if (pageNum > 1) {
            that.setData({ pageNum: that.data.pageNum - 1 })
          }
        }

      })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      view: {
        Height: 1500
      },
      id:options.scene
    })
    this.getGoodsDetail()
    this.getExChangeRecord(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {   

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getExChangeRecord(1)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({ pageNum: this.data.pageNum + 1 })
    this.getExChangeRecord(this.data.pageNum)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})